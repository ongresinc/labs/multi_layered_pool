# Multi Layer Pool Lab


```bash
docker-compose up -d
```

Connect to the pgbouncer-front for testing connection:

```bash
psql -h localhost -p 8432 -Upostgres
```

You can also connect to the PgBouncer Admin console by:

```bash
psql -h localhost -p 8432 -Upostgres pgbouncer
```

Running the basic benchmark is easy as (where time in seconds is the duration of each run):

```bash
bin/spout_traffic [time]
```

> NOTE: this lab is not scoped for performance investigation, it rathers shows all the 
> components configured and provides a sandbox for issuing maintenance tasks,

You can PAUSE and RESUME traffic at any pool by:

```bash
psql -h localhost -p 8432 -Upostgres pgbouncer -c "PAUSE"
```


## Architecture

The flow is as described bellow:


```mermaid
graph TB
  pgbouncer-front1 -->|server_conn 3| lb
  pgbouncer-front2 -->|server_conn 3| lb

  subgraph AppLayer
    App -->|client_conn 1000| pgbouncer-front1
    pgbouncer-front1
  end

  subgraph AppLayer2
    App2 -->|client_conn 1000| pgbouncer-front2
    pgbouncer-front2
  end

  subgraph Cluster["Cluster"]
    lb["Load Balancer"] -->|client_conn 1000| pgbouncer-back1
    lb -->|client_conn 1000| pgbouncer-back2
    pgbouncer-back1 -->|server_conn 6| leader
    pgbouncer-back2 -->|server_conn 6| leader
    leader
  end
  
```


Complex example:

```mermaid
graph TB
        QMS -->|Cluster Endpoint| envoyQMS
        App -->|Cluster Endpoint| envoyApp
        poolFDW --> extDS["External Data Source"]
        poolFDW --> oSG["Other SG Clusters"]

    subgraph Kubernetes["K8S"]
        envoyQMS -.->| 150 Client Conn | pgb1Q
        envoyQMS -.->| 150 Client Conn | pgb2Q
        envoyQMS -.->| 150 Client Conn | pgb3Q
        envoyApp -.->| 100 Client Conn | pgb1
        envoyApp -.->| 100 Client Conn | pgb2
        envoyApp -.->| 100 Client Conn | pgb3
        pgb3Q -->| 4 conn | leader
        pgb2Q -->| 4 conn | leader
        pgb1Q -->| 4 conn | leader
        pgb3 --> | 6 conn | leader
        pgb2 --> | 6 conn | leader
        pgb1 --> | 6 conn | leader
    subgraph QPOOL["Queue Pool Fleet"]
        pgb1Q["PgBouncer1Q"]
        pgb2Q["PgBouncer2Q"]
        pgb3Q["PgBouncer3Q"]
    end
    subgraph APPPOOL["App Pool Fleet"]
        pgb1["PgBouncer1"]
        pgb2["PgBouncer2"]
        pgb3["PgBouncer3"]
    end
    subgraph PG["Patroni Cluster"]
        subgraph Leader["Leader"]
            leader --> poolFDW["FDW Sidecar Pool"]
            poolLE["LE Sidecar Pool"] -.-> |Disabled| leader
        end
        subgraph ReplicaN["ReplicaN"]
            replicaN
            poolRN["RE Sidecar Pool"] --> replicaN
        end
    end

    end
```


## Monitoring 

Nginx provides stats through URL: localhost:9980/nginx_status

Backend Pools:

```bash
docker-compose logs -f pgbouncer_2 | grep "LOG stat"
docker-compose logs -f pgbouncer_1 | grep "LOG stat"
```

Front pools
```bash
docker-compose logs -f pgbouncer-front | grep "LOG stat"
docker-compose logs -f pgbouncer-front2 | grep "LOG stat"
```



